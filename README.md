This project was moved to https://gitlab.ics.muni.cz/muni-kypo/ansible-roles/elk.

# Ansible role - KYPO CRP elk

This role deploys Elasticsearch, Logstash and optionally Kibana behind Nginx HTTPS reverse proxy for KYPO CRP.

## Requirements

* This role requires root access, so you either need to specify `become` directive as a global or while invoking the role.

    ```yml
    become: yes
    ```

## Role parameters

You can override default values of these parameters:

* `kypo_crp_config_dest` - Path, where all configuration will be created.
* `kypo_crp_elk_compose_filename` - Filename for docker-compose template.
* `kypo_crp_elk_kibana_enabled` - If Kibana should be deployed
* `kypo_crp_elk_kibana_internal_url` - Kibana URL on docker network
* `kypo_crp_elk_kibana_username` - Username of Kibana user
* `kypo_crp_elk_kibana_password` - Password of Kibana user
* `kypo_crp_cert_name` - Name of HTTPS certificate
* `kypo_crp_cert_key_name` - Name of HTTPS private key
* `kypo_crp_cert_csr_name` - Name of HTTPS certificate signing request
* `kypo_crp_cert` - Content of HTTPS certificate. If undefined, certificate and private key will be generated.
* `kypo_crp_cert_key` - Content of HTTPS private key. If undefined, certificate and private key will be generated.
* `kypo_crp_docker_services` - Dictionary with settings for Docker services.
* `kypo_crp_docker_network_name` - The name of the Docker network used by syslog-ng container. If not specified, default Docker network will be used.

(default: [see](defaults/main.yml))
